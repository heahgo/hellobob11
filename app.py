#app.py
from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return "hello bob from user143"

@app.route('/add')
def add():
    a = request.args.get('a')
    b = request.args.get('b')
    if a == None or b == None:
        return "no GET!"
    else:
        result = int(a) + int(b)
        return str(result)

@app.route('/sub')
def sub():
    a = request.args.get('a')
    b = request.args.get('b')
    if a == None or b == None:
        return "no GET!"
    else:
        result = int(a) - int(b)
        return str(result)
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8143)