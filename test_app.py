#test_app.py

import requests
import unittest

case = [(1, 2), (-3, 4), (7, 5), (-3, -29), (135, -34)]
server = "http://13.209.15.210:8143"

class TestApp(unittest.TestCase):

    
    def test_add(self):
        add_get =  "/add?a={a}&b={b}"
        for a,b in case:
            url = server + add_get.format(a=a, b=b)
            res = requests.get(url)
            result = int(res.text)
            self.assertEqual(result, a + b)

    def test_sub(self): 
        sub_get =  "/sub?a={a}&b={b}"
        for a,b in case:
            url = server + sub_get.format(a=a, b=b)
            res = requests.get(url)
            result = int(res.text)
            self.assertEqual(result, a - b)

if __name__ == "__main__":
    unittest.main()